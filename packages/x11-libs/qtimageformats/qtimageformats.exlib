# Copyright 2013-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: Various image format plugins"
DESCRIPTION="Supports MNG, TGA, TIFF and WBMP".

LICENCES+=" GPL-2"
MYOPTIONS="jpeg2000 mng tiff
    webp [[ description = [ Build plugin to read and write Google's WebP image format ] ]]
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        jpeg2000? ( media-libs/jasper[>=1.900.1] )
        mng? ( media-libs/libmng[>=1.0.9] )
        tiff? ( media-libs/tiff[>=4.0] )
        webp? ( media-libs/libwebp:= )
"

qtimageformats_src_configure() {
    local qmake_params=(
        $(qt_enable jpeg2000 jasper)
        $(qt_enable mng)
        $(option tiff -system-tiff -no-tiff)
        $(option webp -system-webp -no-webp)
    )

    eqmake -- "${qmake_params[@]}"
}

