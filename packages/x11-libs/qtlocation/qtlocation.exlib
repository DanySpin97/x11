# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: QtLocation, QtPositioning"
DESCRIPTION="
The Qt Positioning API gives developers the ability to determine a position by
using a variety of possible sources, including satellite, or wifi, or text
file, and so on. That information can then be used to for example determine a
position on a map. In addition satellite information can be retrieved and area
based monitoring can be performed.

The Qt Location API enables you to:
- access and present map data,
- support touch gesture on a specific area of the map,
- query for a specific geographical location and route,
- add additional layers on top, such as polylines and circles,
- and search for places and related images.
"

LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
MYOPTIONS="examples gps"

DEPENDENCIES="
    build:
        gps? ( virtual/pkg-config )
    build+run:
        dev-libs/icu:=
        sys-libs/zlib
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        x11-libs/qtserialport:${SLOT}[>=${PV}]
        gps? (
            gnome-platform/GConf
            gps/gypsy
        )
"

if ever at_least 5.13.0-beta2 ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qtxmlpatterns:${SLOT}[>=${PV}]
    "
fi

qtlocation_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    option gps | edo sed -e '/qtCompileTest(gypsy)/s/^/#/' -i ${PN}.pro

    qmake_src_configure
}

